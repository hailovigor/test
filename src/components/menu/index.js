import React, { Component } from 'react';
import './style.css';
import { goToAnchor } from 'react-scrollable-anchor'
import Wide from './wide'
import Small from './small'

class Menu extends Component {
	render() {
		return (
			<div>
				<Wide>
					<p>Menu</p>
					{this.renderItems()}
				</Wide>
				<Small>
					{this.renderItems()}
				</Small>
			</div>
		);
	}

	renderItems() {
		return (
			<ul>
				<li><a onClick={() => goToAnchor('taskAbout')}>О себе</a></li>
				<li><a onClick={() => goToAnchor('taskForm')}>Форма</a></li>
				<li><a onClick={() => goToAnchor('taskTooltip')}>Всплывающая подсказка</a></li>
				<li><a onClick={() => goToAnchor('taskLoader')}>Индикатор загрузки</a></li>
				<li><a onClick={() => goToAnchor('taskWishlist')}>Вишлист</a></li>
			</ul>
		)
	}
};

export default Menu;
