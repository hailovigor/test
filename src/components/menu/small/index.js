import React, { Component } from 'react';
import './style.css';

class MenuForSmall extends Component {
  state = {
    showMenu: false
  }

  showHide = () => {
    const { showMenu } = this.state;
    this.setState({
      showMenu: !showMenu
    })
  }

  render() {
    const { children } = this.props;
    const { showMenu } = this.state;

    return (
      <div className="small-menu-block">
        <div
          onClick={this.showHide}
          className="menu-btn">
          Menu
        </div>
        <div
          style={{display: showMenu ? 'block' : 'none'}}
          className="drop-menu-block">
          {children}
        </div>
      </div>
    );
  }
}

export default MenuForSmall;
