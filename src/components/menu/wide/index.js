import React from 'react';
import './style.css';

const MenuForWide = ({ children }) => (
	<div className="menu-block">
		{children}
	</div>
);

export default MenuForWide;
