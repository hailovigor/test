import React, { Component } from 'react';
import './style.css';
import ScrollableAnchor from 'react-scrollable-anchor';

class TaskForm extends Component {
	state = {
		showHideMoreSocBtns: false
	}

	render() {
		const { showHideMoreSocBtns } = this.state;

		return (
			<ScrollableAnchor id={'taskForm'}>
				<div className="task-block">
					<div>
						<h2>Форма</h2>
						<div id="form">
							<p className="form-title">Регистрация</p>
							<div className="soc-btns">
								<a className="vk">Регистрация из ВКонтакте</a>
								<a className="fb">Регистрация из Facebook</a>
								{showHideMoreSocBtns ? this.renderMoreSocBtns() : null}
								<a
									onClick={this.setShowHideMoreSocBtns}
									className="other-btn">
									{showHideMoreSocBtns ? "Скрыть ∧" : "Другие ∨"}
								</a>
							</div>
							<div className="login-by-email">
								<p className="or-title">или</p>
								<input placeholder="Ваш логин или E-mail"/>
								<input placeholder="Пароль"/>
								<div className="helpers-block">
									<div className="remember-me">
										<input type="checkbox" defaultChecked />
										<p>Запомнить меня</p>
									</div>
									<div className="forgot-password">
										<a>Забыли пароль?</a>
									</div>
								</div>
								<a className="register-btn">Зарегистрироваться</a>
							</div>
						</div>
					</div>
				</div>
			</ScrollableAnchor>
		);
	}

	renderMoreSocBtns = () => (
		<div>
			<a className="google">Регистрация из Google Plus</a>
			<a className="yandex">Регистрация из <span>Яндекс</span></a>
		</div>
	)

	setShowHideMoreSocBtns = () => {
		const { showHideMoreSocBtns } = this.state;
		this.setState({
			showHideMoreSocBtns: !showHideMoreSocBtns
		})
	}
}

export default TaskForm;
