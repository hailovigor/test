import React from 'react';
import './style.css';
import TaskAbout from './taskAbout';
import TaskForm from './taskForm';
import TaskTooltip from './taskTooltip';
import TaskLoader from './taskLoader';
import TaskWishlist from './taskWishlist';

const Tasks = () => (
	<div className="tasks-block">
		<TaskAbout />
		<TaskForm />
		<TaskTooltip />
		<TaskLoader />
		<TaskWishlist />
	</div>
);

export default Tasks;
