import React from 'react';
import './style.css';
import ScrollableAnchor from 'react-scrollable-anchor';
import ya from './src/ya.png';
import copy from 'copy-to-clipboard';

const TaskAbout = () => (
	<ScrollableAnchor id={'taskAbout'}>
		<div className="task-block">
			<div>
				<h2>О себе</h2>
				<div className="main-info-block">
					<div className="photo-block">
						<a href="https://www.instagram.com/hailovigor/" target="_blank">
							<i className="fab fa-instagram"></i>
						</a>
						<img src={ya} />
					</div>
					<div className="about-block">
						<h1>Хайлов Игорь Сергеевич</h1>
						<p>21 год, Санкт-Петербург, м. Елизаровская</p>
						<div className="contact-block">
							<p>
								<a href="tel:+79111399835"><i className="fas fa-mobile-alt"></i></a>
								<a href="whatsapp://send?phone=79111399835"><i className="fab fa-whatsapp"></i></a>
								<a href="tg://resolve?domain=IgorHailov"><i className="fab fa-telegram"></i></a>
								+7 (911) 139-98-35
								<a onClick={() => copy('+7 (911) 139-98-35')}>
									<i className="far fa-copy" style={{ 'color': '#ddd' }}></i>
								</a>
							</p>
							<p>
								<a href="mailto:igo17082401@gmail.com">
									<i className="fas fa-at"></i>
								</a>
								igo17082401@gmail.com
								<a onClick={() => copy('igo17082401@gmail.com')}>
									<i className="far fa-copy" style={{ 'color': '#ddd' }}></i>
								</a>
							</p>
						</div>
					</div>
				</div>
				<div className="about-projects">
					<p>
						1) Первым проектом была доска объявлений для обмена рекламой между сайтами, пабликами и прочими площадками. Писал на Drupal 7, разбирался с html и css и боялся php.
						(перестал платить за домен, показать нечего, да и не стоит).
					</p>
					<p>
						2) Кто-то где-то узнал, что я умею делать сайты. Попросили - сделал: <a href="http://pr-at.ru/" target="_blank">http://pr-at.ru/</a>. Сам сайт был больше, но некоторые услуги были убраны. И интуитивность тут точно страдает.
						HTML + CSS + JQuery
					</p>
					<p>
						3) Начал изучать нативный android. Целью было написать приложение с картой, на которую любой пользователь мог добавлять фото и описание. Но желающих было мало.
						После этого подключил приложение к Instagram и фотографии показывались на карте в реальном времени. Фото с любого концерта, праздника и тд были в вашем распоряжении. Но правила использования API Instagram были изменены и проект закрылся. Около 2500 установок со всего мира. Немного, но на рекламу не потратил ни копейки. Native Android, Yii/Laravel
					</p>
					<p>
						4) Nested To-Do лист для оценки длительности задачи. Подробнее на <a href="https://spark.ru/startup/targist" target="_blank">Spark</a>. Знакомство с ReactJS, но без Redux (было больно).
					</p>
					<p>
						5) <a href="https://spark.ru/startup/reador" target="_blank">Reador</a>. Знакомство с React Native.
					</p>
					<p>
						6) <a href="https://adshrub.firebaseapp.com/" target="_blank">AdShrub</a> <a href="https://spark.ru/startup/adshrub" target="_blank">(spark)</a>. React + Redux + Firebase. Самый большой мой проект на React. Предназначен для ручной группировки ключевых слов, подбора минус-слов и создания объявлений не в Excel. 
					</p>
					<p>
						7) <a href="http://t.me/FavPlaceBot">Бот Telegram</a>. Laravel 5
					</p>
					<p>
						8) <a href="https://play.google.com/store/apps/details?id=com.targist" target="_blank">Targist</a>. Смысл +- как и в 4 пункте. Оценить время задачи, распределить ее объем до дедлайна. В Google Play сейчас самый простой вариант, после были добавлены пермещения, графики и прочее (но еще не доделано до рабочего состояния). Используется React Native/Firebase, появился интерес к статической типизации :). Работает и на IOS, но жалко 100$, чтобы выкладывать.  
					</p>
				</div>
			</div>
		</div>
	</ScrollableAnchor>
);

export default TaskAbout;
