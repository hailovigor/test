import React, { Component } from 'react';
import './style.css';
import ScrollableAnchor from 'react-scrollable-anchor';

class TaskWithlist extends Component {
	state = {
		list: [
			{
				title: 'Node.js',
				done: false
			},
			{
				title: 'react native bridge',
				done: false
			},
			{
				title: 'native ios',
				done: false
			},
			{
				title: 'machine learning',
				done: false
			},
			{
				title: 'Vue.js ?)',
				done: false
			},
			{
				title: 'Jest и пр.',
				done: false
			},
			{
				title: 'Настройку Webpack',
				done: false
			},
		],
		inputValue: '',
	}

	render() {
		const { list, inputValue } = this.state;
		return (
			<ScrollableAnchor id={'taskWishlist'}>
				<div className="task-block">
					<div>
						<h2>Вишлист</h2>
						<div className="whishlist-block">
							<div className="list-block">
								<ul>
									{list.map((item, i) => (
										<Item
											key={i}
											itemData={{id: i, ...item}}
											onDelete={(id) => this.deleteItem(id)}
											onDone={(id) => this.setDone(id)}/>
									))}
								</ul>
							</div>
							<div className="add-block">
								<input
									placeholder="Нужно изучить.."
									value={inputValue}
									onChange={(e) => this.setState({ inputValue: e.target.value })}/>
								<a onClick={this.addItem}>Save</a>
							</div>
						</div>
					</div>
				</div>
			</ScrollableAnchor>
		);
	}

	addItem = () => {
		const { list, inputValue } = this.state;
		const copyList = list.slice();

		if (inputValue.trim() === '')  {
			alert('Вы ничего не ввели!')
			return;
		}

		copyList.push({
			title: inputValue.trim(),
			done: false
		})

		this.setState({
			list: copyList,
			inputValue: ''
		});
	}

	deleteItem = (id) => {
		let copyList = this.state.list.slice();
		copyList.splice(id, 1);

		this.setState({
			list: copyList
		});
	}

	setDone(id) {
		let copyList = this.state.list.slice();
		copyList[id].done = !copyList[id].done;
		this.setState({
			list: copyList
		});
	}
}

const Item = ({ itemData, onDelete, onDone }) => (
	<li className="item-block">
		<input
			type="checkbox"
			value={itemData.done}
			onChange={() => onDone(itemData.id)}
			/>
		<p>{itemData.title}</p>
		<a onClick={() => onDelete(itemData.id)}>	&#10060;</a>
	</li>
)

export default TaskWithlist;
