import React from 'react';
import './style.css';
import ScrollableAnchor from 'react-scrollable-anchor';

const TaskTooltip = () => (
	<ScrollableAnchor id={'taskLoader'}>
		<div className="task-block" id="loader-task">
			<div>
				<h2>Индикатор загрузки</h2>
				<div className="loader-block">
					<div className="loader first-circle">
						<div className="loader second-circle">
						</div>
					</div>
				</div>
			</div>
		</div>
	</ScrollableAnchor>
);

export default TaskTooltip;
