import React from 'react';
import './style.css';
import ScrollableAnchor from 'react-scrollable-anchor';

const TaskTooltip = () => (
	<ScrollableAnchor id={'taskTooltip'}>
		<div className="task-block" id="tooltip-task">
			<div>
				<h2>Всплывающая подсказка</h2>
				<div className="tooltip">Top
					<span className="tooltiptext top">Tooltip text</span>
				</div>
				<div className="tooltip">Bottom
					<span className="tooltiptext bottom">Tooltip text</span>
				</div>
				<div className="tooltip">Left
					<span className="tooltiptext left">Tooltip text</span>
				</div>
				<div className="tooltip">Right
					<span className="tooltiptext right">Tooltip text</span>
				</div>
			</div>
		</div>
	</ScrollableAnchor>
);

export default TaskTooltip;
