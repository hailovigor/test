import React from 'react';
import Menu from '../components/menu';
import Tasks from '../components/tasks';
import './style.css';
import 'bulma/css/bulma.css';

const App = () => (
	<div>
		<Menu />
		<Tasks />
	</div>
);

export default App;
