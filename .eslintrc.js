module.exports = {
    "env": {
        "browser": true,
        "es6": true
    },
    "extends": [ "eslint:all", "plugin:react/recommended"],
    "parserOptions": {
        "ecmaFeatures": {
            "jsx": true
        },
        "ecmaVersion": 2018,
        "sourceType": "module"
    },
    "plugins": [
        "react"
    ],
    "rules": {
        "no-extra-parens": ["error", "all", { "ignoreJSX": "all" }],
        "sort-imports": 0,
        "react/jsx-uses-react": "error",
        "react/jsx-uses-vars": "error",
        "indent": [2, "tab"],
        "no-tabs": 0,
        "linebreak-style": [
            "error",
            "unix"
        ],
        "quotes": [
            "error",
            "single"
        ],
        "semi": [
            "error",
            "always"
        ]
    }
};